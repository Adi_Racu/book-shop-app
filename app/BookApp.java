package app;

import controller.AdminController;
import controller.Login_control;
import controller.RegularController;
import model.Book;
import model.Book_store;
import model.User_data;
import view.AdminView;
import view.RegularView;
import view.start_window;

public class BookApp {

	public static void main(String[] args) {            
        User_data user_model =new User_data();
        start_window start_view = new start_window();
        Book_store store= new Book_store();
        new Login_control(start_view,user_model,store);
        start_view.setVisible(true);
     
	}	
		
	}



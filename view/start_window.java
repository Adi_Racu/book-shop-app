package view;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Book;
import model.User_data;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Observable;

import javax.swing.JTextField;
import java.util.Observable;
public class start_window extends javax.swing.JFrame implements java.util.Observer {
	User_data u;
	
	
	 public void addAdminLoginListener( ActionListener a) {
		 Admin.addActionListener(a);
	    }
	 public void addRegularLoginListener(ActionListener a) {
		 user.addActionListener(a);
	    }
	 public String getName() {
	        String s1 = username.getText();
	        return s1;
	    }
	    
	    public String getPassword() {
	        String s2 = password.getText();
	        return s2;
	    }    

	private   JButton Admin = new JButton("Administrator");
	private   JButton user = new JButton("User");
	private JPanel contentPane;
	private JTextField username;
	private JTextField password;
	public boolean pass;
	public start_window() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 321, 239);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		user.setFont(new Font("Tahoma", Font.PLAIN, 13));
		user.setBounds(16, 77, 89, 23);
		contentPane.add(user);
		
		user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	UserActionPerformed(evt);
            }
        });
		
		
		Admin.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Admin.setBounds(10, 140, 110, 23);
		contentPane.add(Admin);
		
		Admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	AdminActionPerformed(evt);
            }
        });
		
		
		JLabel Optiune = new JLabel("Selecteaza tip");
		Optiune.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Optiune.setHorizontalAlignment(SwingConstants.TRAILING);
		Optiune.setBounds(16, 24, 89, 14);
		contentPane.add(Optiune);	
		
		
		
		username = new JTextField();
		username.setBounds(206, 80, 86, 20);
		contentPane.add(username);
		username.setColumns(10);
		
		password = new JTextField();
		password.setBounds(206, 143, 86, 20);
		contentPane.add(password);
		password.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("UserName");
		lblNewLabel.setBounds(150, 82, 69, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(150, 145, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel raspuns = new JLabel("New label");
		raspuns.setBounds(206, 25, 46, 14);
		contentPane.add(raspuns);
	}
	
	private void AdminActionPerformed(java.awt.event.ActionEvent evt) {                                         
        
    }  
	private void UserActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
    }    
	
	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}
	public void showMessage(String msg){
        JOptionPane.showMessageDialog(this, msg);
    }
	public boolean key(boolean acces) {
   	 this.pass=acces;
   	return pass;
   }
	}
	
	


package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.AdminController;
import model.Book;
import model.Book_store;

public class RegularView extends JFrame implements java.util.Observer {

	Book_store b;
	  public RegularView(Book_store b) {
	      this.b = b;
	     initComponents();
	      b.addObserver(this);
	   //   clear_table();
	      refreshInfo();
	  }
	  
	  private javax.swing.JButton SellBook ;	
	  
	  
	  
	  
	  public void addBookinser(ActionListener a) {
	      SellBook.addActionListener(a);
	  }
	
	 
	  
	  public int getId() {
	      String s = id.getText();
	      return Integer.valueOf(s).intValue();
	  }
	  
	  public String getTitle() {
	      String s1 = title_field.getText();
	      return s1;
	  }
	  
	  public String getAuthor() {
	      String s2 = author_field.getText();
	      return s2;
	  }
	  
	 
	  public void refreshInfo() {
		  DefaultTableModel model_tabel1 = (DefaultTableModel) table.getModel();
		  AdminController.viewAccountsTable(model_tabel1);
		 
	  }
	  public void clear_table() {
		  DefaultTableModel model_tabel1 = (DefaultTableModel) table.getModel();
		  model_tabel1.setRowCount(0);
		 
	  }
	  
		private JPanel contentPane;
		private JTextField id;
		private JTextField title_field;
		private JTextField author_field;
		private JTable table;
		

		/**
		 * Create the frame.
		 */
		public void initComponents() {
			
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 553, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			SellBook = new JButton("SellBook");
			SellBook.setBounds(41, 41, 89, 23);
			contentPane.add(SellBook);
			
			id = new JTextField();
			id.setBounds(100, 111, 118, 20);
			contentPane.add(id);
			id.setColumns(10);
			
			JLabel Id = new JLabel("Id");
			Id.setBounds(41, 115, 63, 14);
			contentPane.add(Id);
			
			title_field = new JTextField();
			title_field.setColumns(10);
			title_field.setBounds(100, 160, 118, 20);
			contentPane.add(title_field);
			
			JLabel lblTitle = new JLabel("title");
			lblTitle.setBounds(41, 164, 63, 14);
			contentPane.add(lblTitle);
			
			author_field = new JTextField();
			author_field.setColumns(10);
			author_field.setBounds(100, 209, 118, 20);
			contentPane.add(author_field);
			
			JLabel lblAuthor = new JLabel("author");
			lblAuthor.setBounds(41, 213, 63, 14);
			contentPane.add(lblAuthor);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(263, 38, 260, 169);
			contentPane.add(scrollPane);
			
			table = new JTable();
			table.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"ID", "Title", "author","price","stock"
				}
			));
			scrollPane.setViewportView(table);
			//DefaultTableModel model_tabel = (DefaultTableModel) table.getModel();
			 // AdminController.viewAccountsTable(model_tabel);
			
			
			SellBook.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e1) {
					clear_table();
					refreshInfo();
				}
			});
			
		}

		@Override
		public void update(Observable o, Object arg) {
			clear_table();
			refreshInfo();
			
		}
		

}

package view;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.AdminController;
import model.Book;
import model.Book_store;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class AdminView extends JFrame implements java.util.Observer {
  Book_store b;
  public AdminView(Book_store b) {
      this.b = b;
     initComponents();
      b.addObserver(this);
    //  clear_table();
    refreshInfo();
  }
  
  private javax.swing.JButton AddBooK ;	
  private javax.swing.JButton DeleteBook ;
  private javax.swing.JButton ModifyBooK ;
  private javax.swing.JButton UpdateBook ;
  
  
  
  
  public void addBookinser(ActionListener a) {
      AddBooK.addActionListener(a);
  }
  public void addDeleteBookListener(ActionListener a) {
	  DeleteBook.addActionListener(a);
  }
  public void addModifyBookListener(ActionListener a) {
	  ModifyBooK.addActionListener(a);
  }
  public void addUpdateBookListener(ActionListener a) {
	  UpdateBook.addActionListener(a);
  }
  
  public int getId() {
      String s = id.getText();
      return Integer.valueOf(s).intValue();
  }
  
  public String getTitle() {
      String s1 = title_field.getText();
      return s1;
  }
  
  public String getAuthor() {
      String s2 = author_field.getText();
      return s2;
  }
  
  public int getPrice() {
      String s3 = price_field.getText();
      return Integer.valueOf(s3).intValue();
  }
  
  public int getStock() {
      String s4 = stock_fileld.getText();
      return Integer.valueOf(s4).intValue();
  }
  
  public void refreshInfo() {
	  DefaultTableModel model_tabel1 = (DefaultTableModel) table.getModel();
	  AdminController.viewAccountsTable(model_tabel1);
  }
  public void clear_table() {
	  DefaultTableModel model_tabel1 = (DefaultTableModel) table.getModel();
	  model_tabel1.setRowCount(0);
	 
  }
	private JPanel contentPane;
	private JTextField id;
	private JTextField title_field;
	private JTextField price_field;
	private JTextField author_field;
	private JTextField stock_fileld;
	private JTable table;
	

	/**
	 * Create the frame.
	 */
	public void initComponents() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 698, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		AddBooK = new JButton("AddBook");
		AddBooK.setBounds(41, 41, 89, 23);
		contentPane.add(AddBooK);
		
		
		DeleteBook = new JButton("DeleteBooK");
		DeleteBook.setBounds(41, 88, 89, 23);
		contentPane.add(DeleteBook);
		
		ModifyBooK = new JButton("ModifyBooK");
		ModifyBooK.setBounds(41, 137, 89, 23);
		contentPane.add(ModifyBooK);
		
		UpdateBook = new JButton("UpdateBook");
		UpdateBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		
		UpdateBook.setBounds(41, 187, 89, 23);
		contentPane.add(UpdateBook);
		
		id = new JTextField();
		id.setBounds(230, 41, 118, 20);
		contentPane.add(id);
		id.setColumns(10);
		
		JLabel Id = new JLabel("Id");
		Id.setBounds(171, 45, 63, 14);
		contentPane.add(Id);
		
		title_field = new JTextField();
		title_field.setColumns(10);
		title_field.setBounds(230, 90, 118, 20);
		contentPane.add(title_field);
		
		JLabel lblTitle = new JLabel("title");
		lblTitle.setBounds(171, 94, 63, 14);
		contentPane.add(lblTitle);
		
		author_field = new JTextField();
		author_field.setColumns(10);
		author_field.setBounds(230, 139, 118, 20);
		contentPane.add(author_field);
		
		JLabel lblAuthor = new JLabel("author");
		lblAuthor.setBounds(171, 143, 63, 14);
		contentPane.add(lblAuthor);
		
		price_field = new JTextField();
		price_field.setColumns(10);
		price_field.setBounds(230, 186, 118, 20);
		contentPane.add(price_field);
		
		JLabel lblPric = new JLabel("price");
		lblPric.setBounds(171, 190, 63, 14);
		contentPane.add(lblPric);
		
		stock_fileld = new JTextField();
		stock_fileld.setColumns(10);
		stock_fileld.setBounds(230, 230, 118, 20);
		contentPane.add(stock_fileld);
		
		JLabel lblStock = new JLabel("stock");
		lblStock.setBounds(171, 234, 63, 14);
		contentPane.add(lblStock);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(386, 41, 260, 169);
		contentPane.add(scrollPane);
		
		
		
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Title", "author","price","stock"
			}
		));
		scrollPane.setViewportView(table);
		//DefaultTableModel model_tabel = (DefaultTableModel) table.getModel();
		 // AdminController.viewAccountsTable(model_tabel);
		
		
		AddBooK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				clear_table();
				refreshInfo();
			}
		});
		
		DeleteBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e2) {
				clear_table();
				refreshInfo();
			}
		});
		
		ModifyBooK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e3) {
				clear_table();
				refreshInfo();
			}
		});
		
	}

	@Override
	public void update(Observable o, Object arg) {
		clear_table();
		refreshInfo();
		
	}
	
	
}

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import model.Book;
import model.Book_store;
import model.Sold_Book;
import model.User_data;
import model.login_mapper;
import view.AdminView;
import view.RegularView;
import view.start_window;

public class Login_control {
	private start_window view;
    private User_data model;
    private Book_store store;
    private Sold_Book book_model;
    private Book book;
    boolean role=false;
    boolean acces=false;
    login_mapper map=new login_mapper();
    public Login_control(start_window view, User_data model,Book_store store) {
        this.view = view;
        this.model = model;
        this.store=store;
        this.view.addAdminLoginListener(new AdminLoginListener());
        this.view.addRegularLoginListener(new RegularLoginListener());
    }
    
    class AdminLoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.create_log(view.getName(),view.getPassword(),1); 
            role=login_mapper.getLoginAdmin(model.get_Name(), model.get_password());
           // role=login_mapper.getLoginAdmin("Gigi", "123");
           if(role==true) {
           view.showMessage("Login succesfully!");
           book = new Book();    
           AdminView aView = new AdminView(store);
           new AdminController(aView, book,store);
          
           
          // new RegularController(rView, model);
          // rView.setVisible(true);
           aView.setVisible(true);
          
           }
           else{
           view.showMessage("Invalid username and/or password!");
           }
       view.key(role);
        }  
        
    class addRegularLoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.create_log(view.getPassword(),view.getPassword(),1);   
            
        }
    }  
  }
    class RegularLoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.create_log(view.getName(),view.getPassword(),1); 
            role=login_mapper.getLoginRegular(model.get_Name(), model.get_password());
           // role=login_mapper.getLoginAdmin("Gigi", "123");
           if(role==true) {
           view.showMessage("Login succesfully!");
           book_model=new Sold_Book();
           RegularView aView = new RegularView(store);
           new RegularController(aView,book_model,store);
          // new RegularController(rView, model);
          // rView.setVisible(true);
           aView.setVisible(true);
          
           }
           else{
           view.showMessage("Invalid username and/or password!");
           }
       view.key(role);
        }  
        
    class addRegularLoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            model.create_log(view.getPassword(),view.getPassword(),1);   
            
        }
    }  
  } 
    
    
    
    
    
    
    
}
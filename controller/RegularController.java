package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import controller.AdminController.Deletebook;
import controller.AdminController.UpdateBookAdd;
import model.Book;
import model.Book_store;
import model.Sold_Book;
import view.AdminView;
import view.RegularView;
public class RegularController {
	private RegularView view;
    private Book model;
    private Book_store store;
    private Sold_Book model_sold;
    public RegularController(RegularView view, Sold_Book model_sold, Book_store store) {
        this.view = view;
        this.model_sold = model_sold;
        this.store=store;
        this.view.addBookinser(new SellBookAdd());
      
    }
    
    class SellBookAdd implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	          int ID=view.getId();
                    String Title=view.getTitle();
                    String Author=view.getAuthor(); 
                model_sold=new Sold_Book(ID, Title,Author);
               store.add_Sold_Book(model_sold);
            try {
            	sellBook(ID, Title, Author);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }   
    public static void sellBook(int id, String title,String author) throws SQLException {
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
		String statement = "UPDATE book SET stock=stock-1  where id=? and title=? and author=?  ";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setInt(1, id);
		prepSt.setString(2, title);
		prepSt.setString(3, author);
		prepSt.executeUpdate();
		}
		catch(Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, insertAccount(Account acc)" + e);
		}
	} 
   
    
    public static DefaultTableModel viewAccountsTable(DefaultTableModel model)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM book";
            PreparedStatement prepSt = con.prepareStatement(sql);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
            	
            	int id = rs.getInt("id");
            	String title = rs.getString("title");
    			String author = rs.getString("author");
    			int price = rs.getInt("price");
    			int stock = rs.getInt("stock");	
    			model.addRow(new Object[]{id,title,author,price,stock});
            }
            rs.close();
            return model;
        } catch (Exception f) {
            System.out.println("Eroare in AccountMapper, viewAccountsTable(DefaultTableModel model)" + f);}
		return model;
	}
    
}

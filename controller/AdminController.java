package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.table.DefaultTableModel;

import model.Book;
import view.AdminView;
import model.Book_store;
public class AdminController {
	private AdminView view;
    private Book model;
    private Book_store store;
    public AdminController(AdminView view, Book model,Book_store store) {
        this.view = view;
        this.model = model;
        this.store=store;
        this.view.addBookinser(new UpdateBookAdd());
        this.view.addDeleteBookListener(new Deletebook());
        this.view.addModifyBookListener(new ModifyBook());
    }
    
    class UpdateBookAdd implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           /* model.(view.getId(), 
                    view.getTitle(), 
                    view.getAuthor(), 
                    view.getPrice(), 
                    view.getStock());*/
        	
        	model=new Book (view.getId(), 
                    view.getTitle(), 
                    view.getAuthor(), 
                    view.getPrice(), 
                    view.getStock());
        	store.addBook(model);
            //Se insereaza modelul carte in baza de date
            try {
				insertBook(model);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
    }
    
    public static void insertBook(Book newBooK) throws SQLException {
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
		String statement = "INSERT INTO book (id,title, author, price,stock) VALUES (?,?,?,?,?)";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setInt(1, newBooK.getId());
		prepSt.setString(2, newBooK.getTitle());
		prepSt.setString(3, newBooK.getAuthor());
		prepSt.setInt(4, newBooK.getPrice());
		prepSt.setInt(5, newBooK.getStock());
		prepSt.executeUpdate();
		}
		catch(Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, insertAccount(Account acc)" + e);
		}
	} 
    
    class Deletebook implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	model=new Book (view.getId(), 
                    view.getTitle(), 
                    view.getAuthor(), 
                    view.getPrice(), 
                    view.getStock());
        	store.addBook(model);
                 int id=view.getId();     
            delete(id);    
        }
    }
    
    public static void delete(int id) {
 		try {
         	DBConnection db = DBConnection.getConnection();
             Connection con = db.connection;
 		String statement = "DELETE FROM book where id=?";
 		PreparedStatement prepSt = con.prepareStatement(statement);
 		prepSt.setInt(1,id);
 		prepSt.executeUpdate();}
 		catch (Exception e) {
             System.out.println("Eroare in Domain, AccountMapper, delete(int id)" + e);}	
 	}
      
    class ModifyBook implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	model=new Book (view.getId(), 
                    view.getTitle(), 
                    view.getAuthor(), 
                    view.getPrice(), 
                    view.getStock());
        	store.addBook(model);     
                 try {
					Modify(view);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            
        }
    }
    
    public static void Modify(AdminView view) throws SQLException {
		try {
			DBConnection db = DBConnection.getConnection();
	        Connection con = db.connection;
		String statement = "UPDATE book SET title=?,  author=?,  price=?,  stock=?  where id=?";
		PreparedStatement prepSt = con.prepareStatement(statement);
		prepSt.setString(1,view.getName());
		prepSt.setString(2, view.getAuthor());
		prepSt.setInt(3,view.getPrice());
		prepSt.setInt(4,view.getStock());
		prepSt.setInt(5,view.getId());
		prepSt.executeUpdate();
		}
		catch(Exception e) {
	        System.out.println("Eroare in Domain, AccountMapper, insertAccount(Account acc)" + e);
		}
	} 
    
    public static DefaultTableModel viewAccountsTable(DefaultTableModel model)
	{
		try {
        	DBConnection db = DBConnection.getConnection();
            Connection con = db.connection;

            String sql = "SELECT * FROM book";
            PreparedStatement prepSt = con.prepareStatement(sql);

            ResultSet rs = prepSt.executeQuery();
            while(rs.next()){
            	
            	int id = rs.getInt("id");
            	String title = rs.getString("title");
    			String author = rs.getString("author");
    			int price = rs.getInt("price");
    			int stock = rs.getInt("stock");	
    			model.addRow(new Object[]{id,title,author,price,stock});
            }
            rs.close();
            return model;
        } catch (Exception f) {
            System.out.println("Eroare in AccountMapper, viewAccountsTable(DefaultTableModel model)" + f);}
		return model;
	}
    
}

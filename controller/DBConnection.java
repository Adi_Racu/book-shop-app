package controller;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	static final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
	static final String DB_URL = "jdbc:mariadb://localhost:3306/proiect_ps";
	static final String USER = "root";
	static final String PASS = "";

	public Connection connection;
	
	public static DBConnection db;

	private DBConnection() {
		try {
		
			System.out.println("Connecting to database...");
			connection = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (Exception e) {
			
			e.printStackTrace();
		}

	}

	public static synchronized DBConnection getConnection() {
		// pentru a ne asigura ca db este instantiat o singura data, indiferent de unde se apeleaza
		if (db == null) {
			db = new DBConnection();
		}
		return db;
	}

}
package model;

import java.sql.*;

import controller.DBConnection;

public class login_mapper {
	   public static boolean getLoginAdmin(String username, String password) {

	        try {
	            DBConnection db = DBConnection.getConnection();
	            Connection con = db.connection;

	            String sql = "SELECT name, password, rol FROM users WHERE (name = ? and password = ?)";
	            PreparedStatement prepSt = con.prepareStatement(sql);

	            prepSt.setString(1, username);
	            prepSt.setString(2, password);

	            ResultSet rs = prepSt.executeQuery();

	            int r = 0;
	            if (rs.next()) {
	                r = rs.getInt("rol");
	            }
	            rs.close();
	            if (r == 1) {
	                return true;
	            } else {
	                return false;
	            }
	        } catch (Exception e) {
	            System.out.println("Eroare in DataSource, LoginMapper, getLoginAdmin(String username, String password)" + e);
	            return false;
	        }
	    }
	   public static boolean getLoginRegular(String username, String password) {

	        try {
	            DBConnection db = DBConnection.getConnection();
	            Connection con = db.connection;

	            String sql = "SELECT name, password, rol FROM users WHERE (name = ? and password = ?)";
	            PreparedStatement prepSt = con.prepareStatement(sql);

	            prepSt.setString(1, username);
	            prepSt.setString(2, password);

	            ResultSet rs = prepSt.executeQuery();

	            int r = 0;
	            if (rs.next()) {
	                r = rs.getInt("rol");
	            }
	            rs.close();
	            if (r == 0) {
	                return true;
	            } else {
	                return false;
	            }
	        } catch (Exception e) {
	            System.out.println("Eroare in DataSource, LoginMapper, getLoginRegular(String username, String password)" + e);
	            return false;
	        }
	    } 
	   
}

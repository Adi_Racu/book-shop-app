package model;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
@SuppressWarnings("deprecation")
public class Book_store extends Observable  {
	  private ArrayList<Book> bookList;
	  private ArrayList<Sold_Book> soldbookList;  
	    public Book_store() {
	       bookList = new ArrayList<Book>();
	       soldbookList = new ArrayList<Sold_Book>(); 
	       setChanged();
	        notifyObservers();
	       
	    }
	    public void addBook(Book b) {
	        bookList.add(b);
	        setChanged();
	        notifyObservers();
	    }
	    public void add_Sold_Book(Sold_Book sb) {
	    	soldbookList.add(sb);
	        setChanged();
	        notifyObservers();
	    }
	    
	    public void dellBook(Book b) {
	    	bookList.remove(b);
	    }
	    
	    
	    
	    
	    
	    
}

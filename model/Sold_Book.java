package model;
@SuppressWarnings("deprecation")
public class Sold_Book extends java.util.Observable  {

	 private int id;
	    private String title;
	    private String author;
	   

	    public Sold_Book() {
	         setChanged();
	        notifyObservers();
	        
	    }
	    
	    public Sold_Book(int id, String title, String author) {
	        this.id = id;
	        this.title = title;
	        this.author = author; 
	         setChanged();
	        notifyObservers();    
	    }
	    
   }